require 'rails_helper'
RSpec.describe "products/show.html.erb", type: :feature do
  describe 'Display product pages correctly(prodduct_name,description,price and title)' do
    let!(:product) { create(:product) }

    before do
      visit potepan_product_path(product.id)
    end
    it "Display title dynamically" do
      expect(page).to have_title "#{product.name} | Potepanec"
    end

    it "Display product info" do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_content product.display_price
    end
  end
end
