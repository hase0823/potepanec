require 'rails_helper'

RSpec.feature "Categories" do
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "display category page with taxons" do
    expect(page).to have_link taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it "link to the product page" do
    expect(page).to have_link product.name
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "display list view when List is pushed" do
    click_on "List"
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    click_on "Grid"
    expect(page).not_to have_content product.description
  end
end
