require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "product page" do
    render_views
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "get show " do
      expect(response).to be_successful
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
