require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:another_product) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'renders the show template' do
      expect(response).to render_template(:show)
    end

    it "assigned proper taxon" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "assigns proper taxonomy" do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it "assigns proper product" do
      expect(assigns(:products)).to contain_exactly product
    end
  end
end
