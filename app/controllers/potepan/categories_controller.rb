class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(:taxons)
    @products = @taxon.products.includes(master: [:default_price, :images])
    @active_type = params[:active_type].presence || 'Grid'
  end
end
